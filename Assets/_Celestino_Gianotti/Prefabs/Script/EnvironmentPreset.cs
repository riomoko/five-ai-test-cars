﻿using UnityEngine;
using UnityEditor;

namespace WOWCELE
{
    [CreateAssetMenu(fileName = "EnvironmentPreset", menuName = "FiveAi/EnvironmentPreset", order = 0)]
    public class EnvironmentPreset : ScriptableObject
    {
        public Material sky;
        public Color fogColor;
        public float fogDensity;

        public void SetEnvironment()
        {
            RenderSettings.skybox = sky;
            RenderSettings.fogColor = fogColor;
            RenderSettings.fogDensity = fogDensity;
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(EnvironmentPreset))]
    public class EnvironmentPresetEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var script = (EnvironmentPreset) target;

            if (GUILayout.Button("Set Environment", GUILayout.Height(40)))
            {
                script.SetEnvironment();
            }
        }
    }
#endif
}