using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace WOWCELE
{
    public class SpedometerAnalogic : MonoBehaviour
    {
        [SerializeField] private Spedometer _spedometer;
        [SerializeField] private float _zeroOffset;
        [SerializeField] private float _200Offset;
        [SerializeField] private Image _pivot;
        private float _rotationParameter;
        [SerializeField] private float test;

        void Start()
        {
            _rotationParameter = (_200Offset - _zeroOffset) / 200.0f;
        }


        void Update()
        {
            //  _rotationParameter = (_200Offset - _zeroOffset) / 200.0f;
            float rot = _spedometer.speedStable * _rotationParameter + _zeroOffset;
            //  float rot = test * _rotationParameter + _zeroOffset;
            _pivot.transform.eulerAngles = new Vector3(0, 0, rot);
        }
    }
}