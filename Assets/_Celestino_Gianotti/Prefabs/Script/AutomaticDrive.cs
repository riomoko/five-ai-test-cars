using System.Collections;
using System.Collections.Generic;
using BezierSolution;
using ScriptableObjectArchitecture;
using Unity.Mathematics;
using UnityEngine;

namespace WOWCELE
{
    public class AutomaticDrive : MonoBehaviour
    {
        [SerializeField] float _cruiseSpeed;
        [SerializeField] float t;
        [SerializeField] private FloatVariable _steer;
        [SerializeField] private FloatVariable _throttle;
        [SerializeField] private Transform _dummy;
        [SerializeField] private Transform _frontCarL;
        [SerializeField] private Transform _frontCarR;
        [SerializeField] private BezierSpline _spline;

        private Spedometer _spedometer;
        private bool radarWarn;

        void Start()
        {
            _spedometer = GetComponent<Spedometer>();
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            CheckPath();
            DriveVariableUpdate();
        }

        void CheckPath()
        {
            Vector3 pointOnLine = new Vector3();
            _spline.FindNearestPointToLine(_frontCarL.position, _frontCarR.position, out pointOnLine, out t, 100000f);
            _dummy.position = pointOnLine;
        }


        public void RadarWarn()
        {
            //      Debug.Log("RADAR WARN");
            radarWarn = true;
        }

        void DriveVariableUpdate()
        {
            float totDist = Vector3.Distance(_frontCarL.position, _frontCarR.position);
            float duDist = Vector3.Distance(_frontCarL.position, _dummy.position);
            float t = ((duDist / totDist) - 0.5f) * 2;
            _steer.Value = t;

            if (!radarWarn)
            {
                if (_spedometer.speedkh < _cruiseSpeed && Mathf.Abs(t) < 0.5f)
                {
                    if (_spedometer.speedkh < _cruiseSpeed * 0.7f)
                    {
                        _throttle.Value = 1.0f;
                    }
                    else
                    {
                        _throttle.Value = 0.5f;
                    }
                }
                else
                {
                    _throttle.Value = 0.0f;

                    if (_spedometer.speedkh < _cruiseSpeed * 0.2f)
                    {
                        _throttle.Value = 0.3f;
                    }
                }
            }
            else
            {
                if (_spedometer.speedkh > 3.0f)
                {
                    _throttle.Value = -0.9f;
                }
                else
                {
                    _throttle.Value = -0.0f;
                }
            }


            radarWarn = false;
        }
    }
}