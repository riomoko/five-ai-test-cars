using System;
using System.Collections;
using System.Collections.Generic;
using ScriptableObjectArchitecture;
using UnityEngine;

public class LightButton : MonoBehaviour
{
    [SerializeField] private bool _lightOn;
    [SerializeField] private BoolGameEvent _lightToggleEvent;

    private void Start()
    {
        ToggleLight();
    }

    public void ToggleLight()
    {
        _lightOn = !_lightOn;
        _lightToggleEvent.Raise(_lightOn);
    }
}