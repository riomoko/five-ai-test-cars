using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WOWCELE;


public class Radar : MonoBehaviour
{
    [SerializeField] private AutomaticDrive _automaticDrive;

    // Start is called before the first frame update
    private void Start()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Car"))
        {
            _automaticDrive.RadarWarn();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Car"))
        {
            _automaticDrive.RadarWarn();
        }
    }
}