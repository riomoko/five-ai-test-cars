﻿using System;
using BezierSolution;
using UnityEngine;

namespace WOWCELE
{
    public class DummySpline : MonoBehaviour
    {
        [SerializeField] private BezierSpline _spline;
        [SerializeField] private float _t;
        [SerializeField] private Transform _car;
        private Transform _transform;

        private void Start()
        {
            _transform = GetComponent<Transform>();
        }

        private void Update()
        {
            _transform.position = _spline.GetPoint(_t);
            _spline.FindNearestPointTo(_car.position, out _t, 100000f);
            _transform.position = _spline.GetPoint(_t);
        }

        private void UpdatePosition(float t)
        {
            _transform.position = _spline.GetPoint(t);
        }
    }
}