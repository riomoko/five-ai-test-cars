using System.Collections;
using System.Collections.Generic;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace WOWCELE
{
    public class ManualDriver : MonoBehaviour
    {
        [SerializeField] private FloatVariable _steer;
        [SerializeField] private FloatVariable _throttle;

        void Start()
        {
        }
        
        void Update()
        {
            DriveVariableUpdate();
        }

        void DriveVariableUpdate()
        {
            _steer.Value = Input.GetAxis("Horizontal");
            _throttle.Value = Input.GetAxis("Vertical");
        }
    }
}