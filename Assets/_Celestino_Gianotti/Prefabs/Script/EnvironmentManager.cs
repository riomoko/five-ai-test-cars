using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace WOWCELE
{
    [Serializable]
    public class EnviromentSet
    {
        public EnvironmentPreset _environmentPreset;
        public List<GameObject> _gameObjectOff;


        [Space(200)] public List<GameObject> _gameObjectOn;


        public void SetEnvironment()
        {
            foreach (var obj in _gameObjectOn)
            {
                obj.SetActive(true);
            }

            foreach (var obj in _gameObjectOff)
            {
                obj.SetActive(false);
            }

            _environmentPreset.SetEnvironment();
        }
    }


    public class EnvironmentManager : MonoBehaviour
    {
        [SerializeField] private List<EnviromentSet> _environments;
        private int _id;

        void Awake()
        {
            _id = 0;
            _environments[_id].SetEnvironment();
        }

        // Update is called once per frame
        public void SetEnvironment(int id)
        {
            _id = id;
            _environments[_id].SetEnvironment();
        }

        public void SetEnvironment()
        {
            _id++;
            if (_id % _environments.Count == 0)
            {
                _id = 0;
            }

            _environments[_id].SetEnvironment();
        }
    }
}