using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WOWCELE
{
    public class Spedometer : MonoBehaviour
    {
        public float speedkh;
        public float speedStable;
        private Vector3 _previousFramePosition;
        private Transform _transform;
        private float _distanceSum;
        private float _timeSum;

        void Start()
        {
            _timeSum = 0;
            _distanceSum = 0;
            _transform = GetComponent<Transform>();
            _previousFramePosition = _transform.position;
        }


        void Update()
        {
            _timeSum += Time.deltaTime;
            _distanceSum += Vector3.Distance(_previousFramePosition, _transform.position);
            if (_timeSum >= 0.25f)
            {
                speedStable = 3.6f * _distanceSum / _timeSum;
                _timeSum = 0;
                _distanceSum = 0;
            }

            speedkh = 3.6f * Vector3.Distance(_previousFramePosition, _transform.position) / Time.deltaTime;
            _previousFramePosition = _transform.position;
        }
    }
}