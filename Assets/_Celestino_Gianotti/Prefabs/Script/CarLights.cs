using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WOWCELE
{
    public class CarLights : MonoBehaviour
    {
        [SerializeField] private GameObject _nightLight;
        [SerializeField] private GameObject _brakeLight;
        [SerializeField] private GameObject _backLight;
        private Spedometer _spedometer;
        private Drive _drive;

        void Start()
        {
            _drive = GetComponent<Drive>();
            _spedometer = GetComponent<Spedometer>();
        }

        // Update is called once per frame
        void Update()
        {
            if (_drive.Throttle() < 0 && _spedometer.speedStable > 5.0f)
            {
                Brake(true);
            }
            else
            {
                Brake(false);
                //Todo make speedometer detect negative speed
            }

            if (_drive.Throttle() < 0 && _spedometer.speedStable < 4.0f)
            {
                Retro(true);
            }
            else
            {
                Retro(false);
            }
        }

        public void NighLight(bool on)
        {
            _nightLight.SetActive(on);
        }

        public void Brake(bool on)
        {
            //   Debug.Log("BRAKE " + on);
            _brakeLight.SetActive(on);
        }

        public void Retro(bool on)
        {
            _backLight.SetActive(on);
        }
    }
}